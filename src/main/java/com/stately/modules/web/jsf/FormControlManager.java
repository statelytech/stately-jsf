package com.stately.modules.web.jsf;

import com.stately.common.model.DateRange;
import com.stately.common.model.Period;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Edwin
 */
@Named(value = "formControlManager")
@SessionScoped
public class FormControlManager implements Serializable 
{
    private boolean enableSaveButton = true;

    public boolean isEnableSaveButton()
    {
        return enableSaveButton;
    }

    public void setEnableSaveButton(boolean enableSaveButton)
    {
        this.enableSaveButton = enableSaveButton;
    }
    
    
    
    
}
